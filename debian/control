Source: basemap
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Emmanuel Arias <eamanu@debian.org>
Build-Depends: cython3,
               debhelper-compat (= 13),
               dh-sequence-python3,
               furo,
               libgeos-dev,
               python3-all,
               python3-all-dev,
               python3-matplotlib,
               python3-netcdf4,
               python3-numpy,
               python3-numpy2-abi0,
               python3-pil,
               python3-pyproj,
               python3-pyshp,
               python3-setuptools,
               python3-sphinx,
               python3-tk,
Standards-Version: 4.6.2
Homepage: http://matplotlib.org/basemap/
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/python-team/packages/basemap.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/basemap

Package: python-mpltoolkits.basemap-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: matplotlib toolkit to plot on map projections (data package)
 The matplotlib basemap toolkit is a library for plotting 2D data on maps in
 Python. It is similar in functionality to the matlab mapping toolbox, the IDL
 mapping facilities, GrADS, or the Generic Mapping Tools. PyNGL and CDAT are
 other libraries that provide similar capabilities in Python.
 .
 Basemap does not do any plotting on its own, but provides the facilities to
 transform coordinates to one of 23 different map projections (using the PROJ.4
 C library). Matplotlib is then used to plot contours, images, vectors, lines or
 points in the transformed coordinates. Shoreline, river and political boundary
 datasets (from Generic Mapping Tools) are provided, along with methods for
 plotting them. The GEOS library is used internally to clip the coastline and
 political boundary features to the desired map projection region.
 .
 Basemap provides facilities for reading data in netCDF and Shapefile formats,
 as well as directly over http using OPeNDAP. This functionality is provided
 through the PyDAP client, and a Python interface to the Shapefile C library.
 .
 Basemap is geared toward the needs of earth scientists, particular
 oceanographers and meteorologists. The author originally wrote Basemap to help
 in his research (climate and weather forecasting), since at the time CDAT was
 the only other tool in Python for plotting data on map projections. Over the
 years, the capabilities of Basemap have evolved as scientists in other
 disciplines (such as biology, geology and geophysics) requested and contributed
 new features.
 .
 This package contains data files for python-mpltoolkits.basemap

Package: python-mpltoolkits.basemap-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: matplotlib toolkit to plot on map projections (documentation)
 The matplotlib basemap toolkit is a library for plotting 2D data on maps in
 Python. It is similar in functionality to the matlab mapping toolbox, the IDL
 mapping facilities, GrADS, or the Generic Mapping Tools. PyNGL and CDAT are
 other libraries that provide similar capabilities in Python.
 .
 Basemap does not do any plotting on its own, but provides the facilities to
 transform coordinates to one of 23 different map projections (using the PROJ.4
 C library). Matplotlib is then used to plot contours, images, vectors, lines or
 points in the transformed coordinates. Shoreline, river and political boundary
 datasets (from Generic Mapping Tools) are provided, along with methods for
 plotting them. The GEOS library is used internally to clip the coastline and
 political boundary features to the desired map projection region.
 .
 Basemap provides facilities for reading data in netCDF and Shapefile formats,
 as well as directly over http using OPeNDAP. This functionality is provided
 through the PyDAP client, and a Python interface to the Shapefile C library.
 .
 Basemap is geared toward the needs of earth scientists, particular
 oceanographers and meteorologists. The author originally wrote Basemap to help
 in his research (climate and weather forecasting), since at the time CDAT was
 the only other tool in Python for plotting data on map projections. Over the
 years, the capabilities of Basemap have evolved as scientists in other
 disciplines (such as biology, geology and geophysics) requested and contributed
 new features.
 .
 This package contains documentation and examples for python-mpltoolkits.basemap

Package: python3-mpltoolkits.basemap
Architecture: any
Depends: python-mpltoolkits.basemap-data (>= ${source:Version}),
         python3-matplotlib,
         python3-packaging,
         python3-pyproj,
         python3-pyshp,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Recommends: python3-pil
Suggests: python3-owslib, python3-scipy
Description: matplotlib toolkit to plot on map projections (Python 3)
 The matplotlib basemap toolkit is a library for plotting 2D data on maps in
 Python. It is similar in functionality to the matlab mapping toolbox, the IDL
 mapping facilities, GrADS, or the Generic Mapping Tools. PyNGL and CDAT are
 other libraries that provide similar capabilities in Python.
 .
 Basemap does not do any plotting on its own, but provides the facilities to
 transform coordinates to one of 23 different map projections (using the PROJ.4
 C library). Matplotlib is then used to plot contours, images, vectors, lines or
 points in the transformed coordinates. Shoreline, river and political boundary
 datasets (from Generic Mapping Tools) are provided, along with methods for
 plotting them. The GEOS library is used internally to clip the coastline and
 political boundary features to the desired map projection region.
 .
 Basemap provides facilities for reading data in netCDF and Shapefile formats,
 as well as directly over http using OPeNDAP. This functionality is provided
 through the PyDAP client, and a Python interface to the Shapefile C library.
 .
 Basemap is geared toward the needs of earth scientists, particular
 oceanographers and meteorologists. The author originally wrote Basemap to help
 in his research (climate and weather forecasting), since at the time CDAT was
 the only other tool in Python for plotting data on map projections. Over the
 years, the capabilities of Basemap have evolved as scientists in other
 disciplines (such as biology, geology and geophysics) requested and contributed
 new features.
 .
 This package contains the Python 3 version of python-mpltoolkits.basemap.
 .
 WARNING: this package is deprecated in favour of cartopy.
